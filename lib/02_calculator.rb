def add(arg1, arg2)
  arg1 + arg2
end

def subtract(arg1, arg2)
  arg1 - arg2
end

def sum(array)
  array.empty? ? 0 : array.reduce(:+)
end
