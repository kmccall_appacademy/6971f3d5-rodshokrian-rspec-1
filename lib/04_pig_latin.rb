def translate(phrase)
  phrase = phrase.split
  phrase.each_with_index do |word, idx|
    result = ""
    consonants = ""
    first_vowel = word.index(/[aeiou]/)
    first_vowel += 1 if word[first_vowel] == "u" && word[first_vowel - 1] == "q"
    consonants = word[0...first_vowel]
    for i in first_vowel...word.length do
      result += word[i]
    end
    phrase[idx] = result + consonants + "ay"
  end
  phrase.join(" ")
end
