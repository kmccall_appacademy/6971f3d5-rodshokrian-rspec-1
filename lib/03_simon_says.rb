def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, repetitions=2)
  new_phrase = phrase
  for i in 1...repetitions do
    new_phrase += " #{phrase}"
  end
  new_phrase
end

def start_of_word(word, num_of_letters)
  start = ""
  for i in 1..num_of_letters do
    start += word[i - 1]
  end
  start
end

def first_word(phrase)
  phrase.split[0]
end

def titleize(title)
  results = title.split
  small_words = ["and", "if", "the", "over", "for"]  
  results.each_with_index do |word, idx|
    results[idx] = word.capitalize unless small_words.include?(word) && idx != 0
  end
  results.join(" ") 
end

